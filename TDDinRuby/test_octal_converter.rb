require 'minitest/autorun'
require_relative 'octal_converter'

describe OctalConverter do
  it 'should return 1 for 1' do
    converter = OctalConverter.new(1)
    result = converter.convert
    assert_equal [1], result
  end

  it 'should return 2 for 2' do
    converter = OctalConverter.new(2)
    result = converter.convert
    assert_equal [2], result
  end

  it 'should return 10 for 8' do
    converter = OctalConverter.new(8)
    result = converter.convert
    assert_equal [1,0], result
  end

  it 'should return 137 for 95' do
    converter = OctalConverter.new(95)
    result = converter.convert
    assert_equal [1,3,7], result
  end

  it 'should return 201 for 129' do
    converter = OctalConverter.new(129)
    result = converter.convert
    assert_equal [2,0,1], result
  end
end
